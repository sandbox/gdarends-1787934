<?php
/**
 * @file
 * Implements the different hook required by Drupal Commerce module to display specific Checkout Panes
 */


/**
 * Form implementation set in commerce_userpoints_discount_commerce_checkout_pane_info().
 */
function commerce_userpoints_discount_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $points = _commerce_userpoints_discount_current_all_points($order);
  $order_total = _commerce_userpoints_discount_order_total($order);
  $points['max_points'] = $points['all_points'] > $order_total ? $order_total : $points['all_points'];

  // Do not return the complete form when points can't be used
  if ($points['max_points'] <= 0) {
    return array('cup_notice' => array(
      '#type' => 'item',
      '#title' => t('Using your credit'),
      '#description' => t("Currently you cannot use your credits."),
    ));
  }

  $pane_form = array('#prefix' => '<div id="cup-slider-status-messages"></div>');

  $pane_form['cup_information'] = array(
    '#type' => 'item',
    '#title' => t('Using your credit'),
    '#description' => t('Enter the amount of credit you wish to use.'),
  );

  $pane_form['cup_credit_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Amount'),
    '#description' => t('Amount of credit to use. The maximum allowed for this order is !points.', array('!points' => $points['max_points'])),
    '#default_value' => $points['current_points'],
    '#size' => 10,
    '#ajax' => array(
      'callback' => 'commerce_userpoints_discount_use_credit_callback_ajax',
      'event' => 'slidestop',
      'keypress' => TRUE,
    ),
  );

  $pane_form['cup_credit_amount_slider'] = array(
    '#type' => 'item',
    '#attached' => array(
      'library' => array(
        array('system', 'ui.slider'),
      ),
      'js' => array(
        array('data' => drupal_get_path('module', 'commerce_userpoints_discount') . '/current_points_slider.js'),
        array(
          'data' => array(
            'commerce_userpoints_discount' => array(
              'current_points' => $points['current_points'],
              'max_points' => $points['max_points'],
              'basePath' => url('commerce_userpoints_discount'),
            ),
          ),
          'type' => 'setting',
        ),
      ),
    ),
  );

  $pane_form['cup_credit_available'] = array(
    '#type' => 'item',
    '#title' => t('Available credit'),
    '#markup' => $points['all_points'],
  );

  $pane_form['cup_submit'] = array(
    '#type' => 'submit',
    '#access' => TRUE,
    '#value' => t('Use credit'),
    '#submit' => array('commerce_userpoints_discount_use_credit_callback'),
  );
  return $pane_form;
}

/**
 * Implements the callback for the checkout pane review form.
 */
function commerce_userpoints_discount_pane_review($form, $form_state, $checkout_pane, $order) {
  // Don't return anything.
  return;

  // Extract the View and display keys from the cart contents pane setting.
  list($view_id, $display_id) = explode('|', variable_get('commerce_userpoints_discount_cart_summary', 'commerce_userpoints_discount_cart_summary|default'));

  return commerce_embed_view($view_id, $display_id, array($order->order_id));
}

/**
 * Checkout pane callback: returns the cart contents pane's settings form.
 */
function commerce_userpoints_discount_pane_settings_form($checkout_pane) {
  $form = array();

  // Build an options array of Views available for the cart contents pane.
  $options = array();

  // Generate an option list from all user defined and module defined views.
  foreach (views_get_all_views() as $view_id => $view_value) {
  // Only include line item Views.
    if ($view_value->base_table == 'commerce_order') {
      foreach ($view_value->display as $display_id => $display_value) {
        $options[check_plain($view_id)][$view_id . '|' . $display_id] = check_plain($display_value->display_title);
      }
    }
  }

  $form['commerce_userpoints_discount_review_pane_view'] = array(
    '#type' => 'select',
    '#title' => t('Cart contents View'),
    '#description' => t('Specify the View to use in the review pane to display the coupons.'),
    '#options' => $options,
    '#default_value' => variable_get('commerce_userpoints_discount_cart_summary', 'commerce_userpoints_discount_cart_summary|default'),
  );

  return $form;
}
