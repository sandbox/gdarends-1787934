(function ($) {
  Drupal.behaviors.commerceUserpointsDiscountCreditSlider = {
    attach: function (context, settings) {
      $( "#edit-commerce-userpoints-discount-cup-credit-amount-slider" ).slider({
        value: Drupal.settings.commerce_userpoints_discount['current_points'],
        max: Drupal.settings.commerce_userpoints_discount['max_points'],
        slide: function(event, ui) {
          $("#edit-commerce-userpoints-discount-cup-credit-amount").val(ui.value);
        },
        stop: function(event, ui) {
          // Only trigger the slidestop event when the amount actually changed
          if (ui.value != Drupal.settings.commerce_userpoints_discount['current_points']) {
            $('#edit-commerce-userpoints-discount-cup-credit-amount').trigger('slidestop');
          }
        },
      });

      $('#edit-commerce-userpoints-discount-cup-submit').remove();
    }
  }
})(jQuery);